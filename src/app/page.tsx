import Product from "@/components/Product";
import { getAllProducts } from "@/utils/product";
import Link from "next/link";

export default async function Home() {
  const products = await getAllProducts();

  return (
    <main className="flex min-h-screen flex-col items-center p-12 gap-4">
      <div>
        <h1 className="text-4xl font-bold text-sky-950">Daftar Produk</h1>
        <p className="text-center">Jumlah produk : {products?.length}</p>
      </div>
      <Link href="/add">
        <button className="py-2 px-4 bg-sky-500 hover:bg-sky-300 hover:text-sky-950 rounded-md text-white font-bold transition-all">
          + Add Product
        </button>
      </Link>
      <div className="flex flex-col items-center justify-center">
        {products ? (
          <Product products={products} />
        ) : (
          <p>
            Product Data is not Loaded, please check your internet or refresh
            the browser
          </p>
        )}
      </div>
    </main>
  );
}
