import ProductCard from "@/components/ProductCard";
import { getProduct } from "@/utils/product";

async function page({ params }: { params: { productId: string } }) {
  const product = await getProduct(Number(params.productId));
  return (
    <main className="flex min-h-screen flex-col items-center p-12 gap-4">
      <h1 className="text-4xl font-bold text-sky-950">Update Product</h1>
      {product ? (
        <ProductCard edit={true} product={product} />
      ) : (
        <p>
          Product with id {params.productId} is not found, please check your
          product id or refresh
        </p>
      )}
    </main>
  );
}

export default page;
