import ProductCard from "@/components/ProductCard";

function page() {
  return (
    <main className="flex min-h-screen flex-col items-center p-12 gap-4">
      <h1 className="text-4xl font-bold text-sky-950">Add Product</h1>
      <ProductCard />
    </main>
  );
}

export default page;
