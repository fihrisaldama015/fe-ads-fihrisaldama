const API_URL = process.env.NEXT_PUBLIC_API_URL;

const getAllProducts = async () => {
  try {
    const res = await fetch(`${API_URL}/api/v1/data`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        nim: "21081010110",
      },
      cache: "no-cache",
      next: {
        tags: ["product"],
      },
    });
    const data = await res.json();
    if (!data) throw new Error("No data found");
    return data.data as Product[];
  } catch (error) {
    console.log("🚀 ~ file: product.ts:21 ~ getAllProducts ~ error:", error);
  }
};

const getProduct = async (productId: number) => {
  try {
    const res = await fetch(`${API_URL}/api/v1/data/${productId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        nim: "21081010110",
      },
      cache: "no-cache",
    });
    const data = await res.json();
    if (!data) throw new Error("No data found");
    return data.data as Product;
  } catch (error) {
    console.log("🚀 ~ file: product.ts:40 ~ getProduct ~ error:", error);
  }
};

const updateProduct = async (
  product: {
    title: string;
    price: number;
    author: string;
    description: string;
  },
  productId: number
) => {
  try {
    const res = await fetch(`${API_URL}/api/v1/data/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        nim: "21081010110",
      },
      cache: "no-cache",
      body: JSON.stringify(product),
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("🚀 ~ file: product.ts:66 ~ error:", error);
  }
};

const addProduct = async (product: {
  title: string;
  price: number;
  description: string;
  author: string;
}) => {
  try {
    const res = await fetch(`${API_URL}/api/v1/data`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        nim: "21081010110",
      },
      cache: "no-cache",
      body: JSON.stringify(product),
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("🚀 ~ file: product.ts:39 ~ addProduct ~ error:", error);
  }
};

const deleteProduct = async (productId: number) => {
  try {
    const res = await fetch(`${API_URL}/api/v1/data/${productId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        nim: "21081010110",
      },
      cache: "no-cache",
    });
    const data = await res.json();
    console.log("🚀 ~ file: product.ts:38 ~ deleteProduct ~ data:", data);
  } catch (error) {
    console.log("🚀 ~ file: product.ts:31 ~ deleteProduct ~ error:", error);
  }
};

export { getAllProducts, deleteProduct, addProduct, getProduct, updateProduct };
