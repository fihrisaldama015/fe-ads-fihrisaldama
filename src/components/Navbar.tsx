import Link from "next/link";

function Navbar() {
  return (
    <nav className="w-full py-4 px-8 flex justify-between text-white bg-sky-300">
      <Link href="/">
        <h1 className="text-sky-950 font-bold">
          TEST CASE - ADS FE MSIB Batch 6
        </h1>
      </Link>
      <p className="text-sky-950 font-medium">Muhamad FIhris Aldama</p>
    </nav>
  );
}

export default Navbar;
