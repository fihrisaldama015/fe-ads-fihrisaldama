"use client";

import s from "@/styles/form.module.css";
import { addProduct, updateProduct } from "@/utils/product";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { FormEvent, useState } from "react";

type ProductCardProps = {
  product?: Product;
  edit?: boolean;
  readOnly?: boolean;
};

const ProductCard = ({ edit, product, readOnly }: ProductCardProps) => {
  const [title, setTitle] = useState<string>(product?.title ?? "");
  const [description, setDescription] = useState<string>(
    product?.description ?? ""
  );
  const [price, setPrice] = useState<string>(product?.price.toString() ?? "");
  const [author, setAuthor] = useState<string>(product?.author ?? "");
  const [loading, setLoading] = useState<boolean>(false);
  const router = useRouter();

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);
    try {
      if (edit && product) {
        await updateProduct(
          { title, description, price: Number(price), author },
          product.id
        );
      } else {
        await addProduct({ title, description, price: Number(price), author });
      }
      router.refresh();
      router.push("/");
      return;
    } catch (error) {
      console.log(
        "🚀 ~ file: ProductCard.tsx:46 ~ handleSubmit ~ error:",
        error
      );
    }
    setLoading(false);
  };

  return (
    <main className="main">
      <div className="card">
        <form className={s.form} onSubmit={handleSubmit}>
          <label htmlFor="title" className={s.label}>
            Title
          </label>
          <input
            type="text"
            name="title"
            id="title"
            className=""
            placeholder="Add your title..."
            value={title}
            required
            disabled={readOnly}
            onChange={(e) => setTitle(e.target.value)}
          />
          <label htmlFor="description" className={s.label}>
            Description
          </label>
          <textarea
            name="description"
            id="description"
            placeholder="Add your description..."
            value={description}
            required
            disabled={readOnly}
            rows={5}
            onChange={(e) => setDescription(e.target.value)}
          />
          <label htmlFor="price" className={s.label}>
            Price
          </label>
          <input
            type="number"
            name="price"
            id="price"
            placeholder="Insert price..."
            value={price}
            required
            disabled={readOnly}
            onChange={(e) => setPrice(e.target.value)}
          />
          <label htmlFor="author" className={s.label}>
            Author
          </label>
          <input
            type="text"
            name="author"
            id="author"
            placeholder="Author..."
            value={author}
            required
            disabled={readOnly}
            onChange={(e) => setAuthor(e.target.value)}
          />
          <button
            type="submit"
            className={`button bg-sky-500 text-white hover:shadow-green-700/30 ${
              readOnly && "hidden"
            }`}
          >
            {loading
              ? "Processing..."
              : edit
              ? "Update Product"
              : "Add Product"}
          </button>
        </form>
        <Link href="/">
          <p className="text-slate-500 mt-2 hover:cursor-pointer hover:text-blue-500">
            &larr; Back
          </p>
        </Link>
      </div>
    </main>
  );
};

export default ProductCard;
