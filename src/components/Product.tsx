"use client";
import styles from "@/styles/index.module.css";
import { deleteProduct } from "@/utils/product";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { confirmAlert } from "react-confirm-alert";

function Product({ products }: { products: Product[] }) {
  const router = useRouter();
  const confirm = (productId: number, name: string) => {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className="card text-center absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
            <h1 className="text-4xl font-bold mb-2 text-red-400">
              Are you sure?
            </h1>
            <p className="text-xl mb-4">
              You want to delete this product below?
            </p>
            <p className="text-4xl mb-8">{name}</p>
            <div className="border-t-2">
              <button
                onClick={onClose}
                className="text-slate-500 font-bold mt-4 mx-4 py-4 px-8"
              >
                Cancel
              </button>
              <button
                onClick={async () => {
                  await deleteProduct(productId);
                  router.refresh();
                  onClose();
                }}
                className="text-red-600 bg-red-200 mt-4 mx-4 py-4 px-8 font-bold rounded-xl"
              >
                Delete!
              </button>
            </div>
          </div>
        );
      },
    });
  };

  return (
    <table className={`${styles.table} table-auto font-normal`}>
      <thead>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Price</th>
          <th>Author</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {products?.map((product: Product, id: number) => (
          <tr key={id} className="hover:bg-slate-50">
            <td>
              <Link href={`product/${product.id}`}>{product.title}</Link>
            </td>
            <td>
              <Link href={`product/${product.id}`}>{product.description}</Link>
            </td>
            <td>
              <Link href={`product/${product.id}`}>{product.price}</Link>
            </td>
            <td>
              <Link href={`product/${product.id}`}>{product.author}</Link>
            </td>
            <td>
              <button className="button bg-yellow-200 text-yellow-700 hover:shadow-yellow-700/30">
                <Link href={`/edit/${product.id}`}>Edit</Link>
              </button>
              <button
                className="button bg-red-200 text-red-700 hover:shadow-red-700/30"
                onClick={() => confirm(product.id, product.title)}
              >
                Delete
              </button>
            </td>
          </tr>
        ))}
        {products?.length === 0 && (
          <tr>
            <td colSpan={4} className="text-center">
              No data.
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

export default Product;
