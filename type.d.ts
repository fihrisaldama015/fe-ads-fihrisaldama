type Product = {
  id: number;
  title: string;
  description: string;
  price: number;
  author: string;
};
